# qrl

simple serverless single-system sequential scheduler

This implements a trivial batch task queue.
It was written more as a demonstration than as production software.
That said, it may be suitable for personal use.

Being serverless, it doesn't need to be installed, all it needs is
a suitable queue directory.
Use several directories for independent queues.

This runs on Linux and MacOS and, most likely, BSDs.

## tasks

Queuing is based on file locks,
specifically full-file exclusive and shared `flock(2)`.
Each task has a numbered lock file in the queue directory,
and holds an exclusive lock on that file.
A task waits for a shared lock on the previous task's lock file.
When a task exits, its exclusive lock gets released,
and the next task wakes up from its shared lock.

The `lslocks` command on Linux shows these locks.

A queue can be shared across hosts
through a shared filesystem that supports locking.
A separate `fstest` command tests filesystem locking.

Deleting the queue directory or moving task lock files can damage the queue.

## process

Instead of being run by a server,
the process that creates a task becomes the task process,
retaining its user identity, privileges, current directory, and environment variables.
In a shared filesystem queue, the task continues to run on the original host.

The `nq` subcommand creates a task lock file
and logs time and task number to the task file.
The process detaches stdio from the terminal and pipes.

*   `stdin` is closed unless it is redirected from a regular file.

*   `stdout` redirects to the task file unless it is already redirected to a regular file.

*   `stderr` redirects to the task file,

The process detaches from the terminal session by forking
and changing its session id.

Once previous locks are released,
the detached process check to see if it has been
cancelled by the `dq` command deleting the task file.
If so, it simply exits, releasing its own lock.
Otherwise the detached process `exec`s its arguments to run the task.
When it exits, its own lock is released and the next task proceeds.

Redirecting and detaching from the terminal are similar to `nohup(1)`.

Tasks can also wait and run in foreground without redirection.

Killing a waiting task process can damage the queue.
Tasks do not survive system restart.

## usage

    qrl.py [-q QUEUEDIR] subcommand

has subcommands to manage the queue.

The default queue is `"./Q"` or the environment variable `$QRL`.
Multiple directories give you multiple queues.
The directory filesystem must support `flock`.
Most filesystems don't support `flock` across system boundaries.

### subcommands

    nq [--fg] [--] [arg...]

enqueues a task to exec `arg...`
In most cases "--" should be used to separate task args from nq flags.
An empty task doesn't run anything but holds a position in line.

The `--fg` option waits and runs the task in foreground instead of detaching.

    q [task...]

lists uncompleted tasks in the task list,
or for all uncompleted tasks if no list is given.

    dq [task...]

cancels tasks,
or all waiting tasks if no list is given.
Waiting process continue to hold their place in the queue,
but exit without running their task.

Once a task has started running, it cannot be cancelled, but can safely be killed.

    wq [task...]

waits for the tasks to be completed,
or all waiting tasks if no list is given.

## examples

Suppose we have some large ipv4 address lists to sort,
and only enough `/tmp` to sort one at a time.

    export LC_ALL=C
    ./qrl.py nq sort -V <hosts > hosts-sorted
    ./qrl.py nq sort -V <routers > routers-sorted
    ./qrl.py nq sort -V <dark > dark-sorted

The sorts run sequentially in the environment with `LC_ALL=C`,
and with their stdin and stdout redirections.

A foreground task waits in the queue as well, and can be used as an alert.

    ./qrl.py nq --fg ls -l *-sorted

## fstest

    fstest.py [-q QUEDIR]

tests a queue directory for suitable `flock` semantics.

    fstest.py [-q QUEDIR] a | b

tests a shared queue across hosts.
One host runs `fstest a`, the other runs `fstest b`.

## installation

No need to splatter code around the file system.
Just clone the repository to a directory and do one of

    alias qrl=.../qrl.py

makes an alias to run from the clone directory.

    (echo '#!/usr/bin/env python'
    zip - __main__.py flock.py fork.py fs.py state.py) \
    | cat > qrl

packages the source as a python executable zip file.
`echo` adds a "shebang" to run the executable with python.
`zip -` makes a zip archive on stdout, with `__main__.py` as the main program.
`cat` is necessary to keep `zip` from rewinding the output and erasing the `echo`

## see also

`nohup` runs detached commands, but without queueing.

`ts` (aka `tsp`) is a more capable task scheduler.

`at` comes with a `batch` command, but it schedules for system load.

`lp` can queue more than print jobs.
