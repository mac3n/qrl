"""
    ignore the python lock mess
    https://chris.improbable.org/2010/12/16/everything-you-never-wanted-to-know-about-file-locking/
    and create our own mess for now using full file locking `flock(2)`
    we want flock(2) semantics.
        full file lock
        lock attached to opened fd and carried by dup2
        lock released by close
    tests use separate locks on multiple opens, but we don't reallly need that
"""

import ctypes
import os

# opcode bits
# these are valid for Linux & BSD (MacOS)
LOCK_SH = 1
LOCK_EX = 2
LOCK_NB = 4
LOCK_UN = 8

# limited portability
LIBS = dict(
        linux="libc.so.6",
        darwin="/usr/lib/libc.dylib",
        freebsd="libc.so.7")
uname = os.uname()[0].lower()
LIBC = ctypes.cdll.LoadLibrary(LIBS[uname])


def flock(fd, opcode):
    "thinnest wrapper."
    return LIBC.flock(ctypes.c_int(fd), ctypes.c_int(opcode))
