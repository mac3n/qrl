"""
    process management
    we use fork/setsession/fork to detach from the terminal session
"""
import os


# standard fds
STDIN, STDOUT, STDERR = 0, 1, 2


def env(key):
    "look up ENV config."
    return os.environ.get(key)


# we use os._exit() post-fork, instead of sys.exit to avoid hooks
# especially nosetests

def detach():
    "Detach from cli to go headless and return final pid."
    # first fork to detach as process group leader
    pid = os.fork()
    if pid:
        # lose parent
        os._exit(0)
    # set new session
    os.setsid()
    # second fork to detach from session
    pid = os.fork()
    if pid:
        # lose parent
        os._exit(0)
    return os.getpid()


def argv(*argv):
    "Exec the task."
    if not argv:
        # null task
        os._exit(0)
    os.execvp(argv[0], argv)
    # serious fail
    os._exit(-1)
