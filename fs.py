"""
    file number operations
    we don't do buffered io
    and we do a lot of low-level state operations
    we also need it to close when we call `close()`
"""

import os
import stat
import flock


class FD(object):
    "Task file descriptor."
    def __init__(self, fd):
        self.fd = fd

    def links(self):
        "Check if still in directory."
        return os.fstat(self.fd)[stat.ST_NLINK]

    def disk(self):
        "Check if regular file, mostly not a tty and not a pipe."
        s = os.fstat(self.fd)
        return stat.S_ISREG(s[stat.ST_MODE])

    def lock(self, exclusive=None, test=None):
        "Lock, close to release."
        op = flock.LOCK_EX if exclusive else flock.LOCK_SH
        if test:
            op |= flock.LOCK_NB
        return flock.flock(self.fd, op) == 0

    def read(self, size):
        "Read the task file log."
        return os.read(self.fd, size)

    def write(self, log):
        "Log to a task."
        os.write(self.fd, log)

    def alias(self, fd1):
        "Reopen ourself into another fd."
        os.dup2(self.fd, fd1)

    def reopen(self, fd1):
        "Rename ourself to another fd."
        self.alias(fd1)
        self.close()
        self.fd = fd1

    def close(self):
        os.close(self.fd)

    def __enter__(self):
        "As context for temporary use."
        return self

    def __exit__(self, *args):
        "Don't handle exceptions, but close."
        self.close()
        return None
