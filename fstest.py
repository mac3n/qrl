#!/usr/bin/env python
"""
    Test filesystem lock support.
"""
from __future__ import print_function

import os
import sys
import time

import flock


def flocktests(test, path):
    "Test locking on distinct opens and return failure."
    fails = 0
    # open twice
    fd0, fd1 = os.open(path, os.O_RDONLY), os.open(path, os.O_RDONLY)
    flock.flock(fd0, flock.LOCK_EX)
    # mutex
    if (
            flock.flock(fd1, flock.LOCK_EX | flock.LOCK_NB) == 0 or
            flock.flock(fd1, flock.LOCK_SH | flock.LOCK_NB) == 0):
        # mutex failure
        print(test, "no exclusion", path, file=sys.stderr)
        fails += 1
    os.close(fd0)
    # close release
    if flock.flock(fd1, flock.LOCK_SH | flock.LOCK_NB) != 0:
        print(test, "no release", path, file=sys.stderr)
        fails += 1
    # reopen and share
    fd0 = os.open(path, os.O_RDONLY)
    if flock.flock(fd0, flock.LOCK_SH | flock.LOCK_NB) != 0:
        print(test, "no share", path, file=sys.stderr)
        fails += 1
    os.close(fd0)
    os.close(fd1)
    return fails


def flockrm(path):
    "Test lock on disappearing file."
    fails = 0
    fd0, fd1 = os.open(path, os.O_RDONLY), os.open(path, os.O_RDONLY)
    os.unlink(path)
    if (
            flock.flock(fd0, flock.LOCK_EX) != 0 or
            flock.flock(fd1, flock.LOCK_EX | flock.LOCK_NB) == 0):
        print("no lock on disappearing file", path, file=sys.stderr)
        fails += 1
    os.close(fd0)
    os.close(fd1)
    return fails


# we use file existance to signal across systems with shared fs
# this relies on proper lock state being propagated


def syncfile(origin, path):
    "Synchronize on file creation."
    if origin:
        fd = os.open(path, os.O_CREAT | os.O_RDWR)
        print("make", path, file=sys.stderr)
    else:
        print("wait", path, end="\t", file=sys.stderr)
        while True:
            try:
                fd = os.open(path, os.O_RDONLY)
            except OSError:
                time.sleep(1)
                print(".", end="", file=sys.stderr)
                sys.stderr.flush()
                continue
            break
        print(file=sys.stderr)
    return fd


def syncrmfile(origin, path):
    "Synchronize on file removal."
    if origin:
        os.unlink(path)
        print("rm", path, file=sys.stderr)
    else:
        print("wait rm", path, end="\t", file=sys.stderr)
        try:
            while True:
                os.close(os.open(path, os.O_RDONLY))
                time.sleep(1)
                print(".", end="", file=sys.stderr)
                sys.stderr.flush()
        except OSError:
            pass
        print(file=sys.stderr)


def flocksys(a, b, qpath):
    "Shared filesystem tests, using file create/delete to signal."
    fails = 0
    apath, bpath = "%s/testa" % qpath, "%s/testb" % qpath
    # directory exclusion
    qd = os.open(qpath, os.O_RDONLY)
    if a:
        # a locks dir and creates A
        flock.flock(qd, flock.LOCK_EX)
    ad = syncfile(a, apath)
    if b:
        # b waits on A, checks lock
        if flock.flock(qd, flock.LOCK_EX | flock.LOCK_NB) == 0:
            print("no directory exclusion", file=sys.stderr)
            fails += 1
    if b:
        # b releases and create B
        os.close(qd)
    bd = syncfile(b, bpath)
    if a:
        if flock.flock(qd, flock.LOCK_EX | flock.LOCK_NB) != 0:
            print("no directory release", file=sys.stderr)
            fails += 1
        os.close(qd)
    # resync delete
    syncrmfile(a, apath)
    syncrmfile(b, bpath)
    os.close(ad)
    os.close(bd)
    # file locking a locks B, b locks A
    ad = syncfile(a, apath)
    if b:
        flock.flock(ad, flock.LOCK_EX)
    bd = syncfile(b, bpath)
    if a:
        flock.flock(bd, flock.LOCK_EX)
    syncrmfile(a, apath)
    syncrmfile(b, bpath)
    # test locks
    if a:
        if flock.flock(ad, flock.LOCK_SH | flock.LOCK_NB) == 0:
            print("no file exclusion", file=sys.stderr)
            fails += 1
    if b:
        if flock.flock(bd, flock.LOCK_SH | flock.LOCK_NB) == 0:
            print("no file exclusion", file=sys.stderr)
            fails += 1
    # release and wait
    if a:
        # a release B lock
        os.close(bd)
    if b:
        # b wait for release, release A lock
        flock.flock(bd, flock.LOCK_SH)
        os.close(bd)
        os.close(ad)
    if a:
        # a wait for A lock release
        flock.flock(ad, flock.LOCK_SH)
        os.close(ad)
    return fails

if __name__ == "__main__":

    usage = "%s [-q DIR] [a | b]" % sys.argv[0]
    args = sys.argv[1:]

    qpath = os.environ.get("QRL") or "./Q"
    fails = 0
    if args and args[0] == '-q':
        args.pop(0)
        qpath = args.pop(0)
    print("testing:", qpath, file=sys.stderr)
    if not args:
        # single system test
        fails += flocktests("directory locking:", qpath)
        fpath = "%s/testlock" % qpath
        os.close(os.open(fpath, os.O_CREAT | os.O_RDWR))
        fails += flocktests("file locking:", fpath)
        fails += flockrm(fpath)
    elif args[0] == "a":
        fails += flocksys(1, 0, qpath)
    elif args[0] == "b":
        fails += flocksys(0, 1, qpath)
    else:
        print(usage, file=sys.stderr)
        sys.exit(-1)
    if fails:
        print("failed", file=sys.stderr)
        sys.exit(-1)
    print("passed")
