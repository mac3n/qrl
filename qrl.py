#!/usr/bin/env python
"""
    simple serverless single-system sequential tasks
"""
from __future__ import print_function

import sys
import time

import fork
import fs
import state


# not safe to use stdio buffering here
# while we muck about with forking and file remapping

# our tasks
Q = state.State(fork.env("QRL") or "./Q")


class Taskd(fs.FD):
    "Task id & FD."
    def __init__(self, fd, task):
        fs.FD.__init__(self, fd)
        self.task = task
        self.last = self.lastd = None

    def log(self, msg, *args):
        "Log to task file."
        msg %= args
        self.write(b"%.3f\t%s\n" % (time.time(), msg.encode()))

    def after(self, task, fd):
        "Attach previous blocking task."
        self.last, self.lastd = task, fd

    def wait(self):
        "Wait our turn."
        if self.lastd is not None:
            # wait for previous task to unlock
            self.lastd.lock()
            self.lastd.close()


def qfds(tasks):
    "Yield open tasks and their fds."
    for task in tasks:
        try:
            yield (task, Q.open(task))
        except OSError:
            print("no task", task, file=sys.stderr)


def q(tasks):
    "List live tasks."
    # lock queue state
    with fs.FD(Q.dir()) as qd:
        qd.lock(exclusive=True)
        # recentmost live tasks
        for (task, fd) in qfds(sorted(tasks, reverse=True)):
            # test shared lock on task
            with fs.FD(fd) as td:
                if td.lock(test=True):
                    # task is no longer locked
                    break
                # show first log line
                print(task, td.read(4096).split("\n", 1)[0], sep="\t")


def dq(tasks):
    "Remove tasks - they still wait in line, but don't run."
    # lock state
    with fs.FD(Q.dir()) as qd:
        qd.lock(exclusive=True)
        for (task, fd) in qfds(sorted(tasks, reverse=True)):
            with fs.FD(fd) as td:
                if td.lock(test=True):
                    # task is no longer locked
                    break
                Q.pop(task)
                print("cancel", task, sep="\t", file=sys.stderr)


def wq(tasks):
    "Wait for tasks in order."
    for (task, fd) in qfds(sorted(tasks, reverse=False)):
        # test shared lock on task
        with fs.FD(fd) as td:
            print(task, end="\t", file=sys.stderr)
            if not td.lock(test=True):
                print("wait...", end="\t", file=sys.stderr)
                sys.stderr.flush()
                td.lock()
            print("done", file=sys.stderr)


def nq(argv):
    "Add new task, return task."
    argstr = " ".join(argv)
    # lock state
    with fs.FD(Q.dir()) as qd:
        qd.lock(exclusive=True)
        tasks = Q.tasks()
        if not tasks:
            # no waiting
            this, last = 0, None
        else:
            # behind last task
            last = max(tasks)
            this = last + 1
        # create task file and own it
        thisd = Taskd(Q.add(this), this)
        thisd.lock(exclusive=True)
        # log line for display
        thisd.log("%s", argstr)
        if last is None:
            thisd.log("add task %d first", this)
        else:
            thisd.log("add task %d after %d", this, last)
            thisd.after(last, fs.FD(Q.open(last)))
        print(this, argstr, sep="\t", file=sys.stderr)
    return thisd


def detach(thisd):
    "detach terminal, stderr and process."
    sys.stdout.flush()
    sin, sout = fs.FD(fork.STDIN), fs.FD(fork.STDOUT)
    # log changes before switching stderr
    if not sin.disk() or not sin.links():
        # stdin is not a disk file
        print(thisd.task, "close stdin", sep="\t", file=sys.stderr)
        sin.close()
    if not sout.disk() or not sout.links():
        # stdout is not a disk file
        print(thisd.task, "divert stdout to log", sep="\t", file=sys.stderr)
        thisd.alias(fork.STDOUT)
    # task log becomes stderr
    sys.stderr.flush()
    thisd.reopen(fork.STDERR)
    # detach from foreground
    pid = fork.detach()
    thisd.log("detached pid %d", pid)


def run(thisd, argv):
    "Run unless removed."
    if not thisd.links():
        thisd.log("cancel")
    else:
        # become task
        thisd.log("%s", " ".join(argv))
        fork.argv(*argv)


# command-line
if __name__ == "__main__":

    import argparse

    def parse():
        "Parse subcommands."
        p = argparse.ArgumentParser(description=__doc__)
        p.add_argument("-q", metavar="DIR", help="queue directory.")
        s = p.add_subparsers(dest="op", help="actions")
        # queue state
        q = s.add_parser("q", help="check task status")
        q.add_argument("tasks", metavar="task", type=int, nargs="*")
        # enqueue
        nq = s.add_parser("nq", help="add task")
        nq.add_argument("--fg", action="store_true", help="run in foreground")
        nq.add_argument("argv", metavar="arg", nargs="*", help="command argv")
        # dequeue
        dq = s.add_parser("dq", help="cancel task")
        dq.add_argument("tasks", metavar="task", type=int, nargs="*")
        wq = s.add_parser("wq", help="wait for task")
        wq.add_argument("tasks", metavar="task", type=int, nargs="*")
        return p.parse_args()

    args = parse()
    if args.q:
        Q = state.State(args.q)
    if args.op == "q":
        q(args.tasks or Q.tasks())
    elif args.op == "dq":
        dq(args.tasks or Q.tasks())
    elif args.op == "wq":
        wq(args.tasks or Q.tasks())
    elif args.op == "nq":
        thisd = nq(args.argv)
        if not args.fg:
            detach(thisd)
        if thisd.last is not None:
            thisd.log("wait")
            thisd.wait()
        thisd.log("ready")
        run(thisd, args.argv)
