"""
    Manage queue state in a directory.
    Tasks are named with an integer counter.
"""


import os


class State(object):
    "Directory of task files."

    def __init__(self, q):
        "Initialize with queue directory."
        self.q = q

    def dir(self):
        "Return fd for the full state, for atomic queue ops."
        return os.open(self.q, os.O_RDONLY)

    def path(self, task):
        "Pathname for task."
        return "%s/%d" % (self.q, task)

    def open(self, task):
        "Get fd for another task."
        return os.open(self.path(task), os.O_RDONLY)

    def add(self, task):
        "Add new a task and return fd. We write to this file"
        return os.open(self.path(task), os.O_CREAT | os.O_RDWR, 0o666)

    def pop(self, task):
        "Remove task to disable execution."
        os.unlink(self.path(task))

    def tasks(self):
        "Return most-recent-ordered list of tasks in the queue."
        tasks = []
        for name in os.listdir(self.q):
            try:
                t = int(name)
            except ValueError:
                continue
            tasks.append(t)
        return tasks
