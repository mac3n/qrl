"""
    low-level unit tests
"""

from __future__ import print_function

import os
import sys
import tempfile

import flock
import fork
import fs
import state


def test_flock():
    "Test flock."
    # open same file twice
    tmp1 = tempfile.NamedTemporaryFile(prefix="flock")
    tmp2 = open(tmp1.name)
    # exclusion
    err = flock.flock(tmp1.fileno(), flock.LOCK_EX)
    assert not err
    err = flock.flock(tmp2.fileno(), flock.LOCK_SH | flock.LOCK_NB)
    assert err
    # release
    flock.flock(tmp1.fileno(), flock.LOCK_UN)
    # share
    err = flock.flock(tmp1.fileno(), flock.LOCK_SH)
    assert not err
    err = flock.flock(tmp2.fileno(), flock.LOCK_SH | flock.LOCK_NB)
    assert not err


def test_fs():
    "Test fd operations."
    # regular file
    tmp = tempfile.NamedTemporaryFile(prefix="test-fs")
    fd = fs.FD(os.open(tmp.name, os.O_RDWR))
    assert fd.links() == 1
    assert fd.disk()
    assert fd.lock(exclusive=True)
    fd.write(b"oK\n")
    # second reference
    rd = fs.FD(os.open(tmp.name, os.O_RDONLY))
    assert rd.read(16) == b"oK\n"
    assert not rd.lock(exclusive=True, test=True)
    # duplicate first reference inherits lock
    fd.alias(rd.fd)
    assert rd.lock(exclusive=True, test=True)
    rd.close()
    # disappearing file
    del tmp
    assert not fd.links()
    fd.close()
    # not a regular file
    xd = fs.FD(os.open("/dev/null", os.O_RDONLY))
    assert not xd.disk()
    xd.close()


def test_with_fd():
    with fs.FD(os.open("/dev/null", os.O_RDONLY)) as xd:
        with fs.FD(os.open("/dev/null", os.O_RDONLY)) as sd:
            # lock second open
            assert sd.lock(exclusive=True)
            # can't lock first
            assert not xd.lock(exclusive=True, test=True)
        # after close, lock first
        assert xd.lock(exclusive=True)


def test_state():
    "Test queue state."
    tmpd = tempfile.mkdtemp(prefix="test-state")
    q = state.State(tmpd)
    assert q.path(0) == "%s/0" % tmpd
    # add task files
    os.close(q.add(0))
    os.close(q.add(7))
    assert q.tasks() == [7, 0]
    # del task files
    for t in q.tasks():
        q.pop(t)
    os.rmdir(tmpd)


def test_exec():
    "Test forked exec."
    # pipe to forked
    (pr, pw) = os.pipe()
    # exec in a fork
    pid = os.fork()
    if not pid:
        # child execs with stdout to pipe
        fs.FD(pw).alias(fork.STDOUT)
        fork.argv("/bin/echo", "oK")
        assert False
    # parent reads pipe
    with fs.FD(pr) as rd:
        r = rd.read(1024)
        print("forked:", r, file=sys.stderr)
        assert r == b"oK\n"


def test_detach():
    # pipe to forked
    (pr, pw) = os.pipe()
    # detach in a fork
    pid = os.fork()
    if not pid:
        # child detaches
        pid1 = fork.detach()
        with fs.FD(pw) as wd:
            wd.write(b"%d\n%d\n%d\n" % (pid1, os.getpgrp(), os.getsid(pid1)))
            fork.argv()
    # parent reads new pid, pgrp, sid
    os.close(pw)
    with fs.FD(pr) as rd:
        pid0, pgrp0 = os.getpid(), os.getpgrp()
        sid0 = os.getsid(pid0)
        print("parent", "pid:", pid0, "pgrp:", pgrp0, "sid:", sid0, file=sys.stderr)
        (pid1, pgrp1, sid1) = rd.read(1024).strip().split(b"\n")
        print("detach", "pid:", pid1, "pgrp:", pgrp1, "sid:", sid1, file=sys.stderr)
        assert pid0 != pid1
        assert pgrp0 != pgrp1
        assert sid0 != sid1


if __name__ == "__main__":
    test_flock()
    test_fs()
    test_with_fd()
    test_state()
    test_exec()
    test_detach()
