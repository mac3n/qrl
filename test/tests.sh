#!/usr/bin/env bash

# fail on unguarded nonzero exit
set -e
trap "echo failed" ERR
# tasks from env
QRL=${QRL:-./Q}

# make a test program with opportunity for interference
cat > $QRL/test.py << 'EOF'
from __future__ import print_function
import sys, time
for _ in xrange(int(sys.argv[1])):
    time.sleep(1)
    print(sys.argv[2], end="")
    sys.stdout.flush()
print()
EOF

# test result
cat > $QRL/testgood << 'EOF'

1
22
333
4444
55555
666666
7777777
88888888
999999999
EOF

echo "run in foreground with redirection"
rm -f $QRL/testput
for i in $(seq 0 9); do ./qrl.py nq --fg python $QRL/test.py $i $i >> $QRL/testput; done
diff $QRL/testgood $QRL/testput && echo oK
rm -f $QRL/testput

echo "run in background"
for i in $(seq 0 9); do ./qrl.py nq python $QRL/test.py $i $i >> $QRL/testput; done
# wait
./qrl.py wq
diff $QRL/testgood $QRL/testput && echo oK
rm -f $QRL/testput

echo "no redirection"
for i in $(seq 0 9); do ./qrl.py nq python $QRL/test.py $i $i ; done
# wait
./qrl.py wq
tasks=$(ls -rt $QRL/[0-9]* | tail -n 10)
tail -qn1 $tasks > $QRL/testput
diff $QRL/testgood $QRL/testput && echo oK
rm -f $QRL/testput

echo "parallel submit"
for i in $(seq 0 9); do ./qrl.py nq python $QRL/test.py $i $i >> $QRL/testput& done
# wait
jobs
wait
jobs
./qrl.py wq
diff $QRL/testgood <(sort $QRL/testput) && echo oK

echo "passed"
rm $QRL/test*

